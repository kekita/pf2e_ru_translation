.. include:: /helpers/roles.rst


.. rst-class:: creature-details

Тихий преследователь (Silent Stalker)
============================================================================================================

.. sidebar:: |lore|

	.. rubric:: Безмолвное суеверие (Silent Superstition)
	
	**Источник**: Book of the Dead pg. 141
	
	**Геб**: Удивительно, как люди не понимают самых элементарных фактов о магии мертвых.
	Абсолютная тишина некоторых ревенантов заставила невежественных глупцов поверить, что они боятся звуков.
	Люди, исповедующие эту глупость, утверждают, что чтобы держать преследователя на расстоянии, можно просто уйти в более шумное место, например, в таверну, полную галдящих посетителей.
	По правде, ничто не может остановить тихого преследователя, кроме как уничтожить его или убить его убийцу.


Если персону предали и убили так, что его голос был заглушен (например, при удушении возлюбленным или близким доверенным лицом), она может вернуться в виде тихого преследователя.
Тихий преследователь - это ужасающее, извращенное подобие той личности, которым он был при жизни, с одним очевидным отличием - у тихого преследователя нет рта, а просто гладкий участок плоти там, где он должен быть.
Звуки вокруг тихого преследователя подавляются, но когда кто-то замечает тишину, часто бывает уже слишком поздно.



.. rst-class:: creature
.. _bestiary--Silent-Stalker:

Тихий преследователь (`Silent Stalker <https://2e.aonprd.com/Monsters.aspx?ID=1894>`_) / Существо 13
------------------------------------------------------------------------------------------------------------

- :alignment:`ПН`
- :size:`маленький`
- нежить

**Источник**: Book of the Dead pg. 141

**Восприятие**: +24;
:ref:`ночное зрение <cr_ability--Darkvision>`,
ощущение убийцы

**Языки**: 
один любой, на котором говорил его убийца (обычно Всеобщий, не может говорить)

**Навыки**:
Атлетика +27,
Запугивание +24,
Скрытность +28

**Сил** +8,
**Лвк** +5,
**Тел** +3,
**Инт** +0,
**Мдр** +5,
**Хар** +3


**Ощущение убийцы (Sense Murderer)**
(:t_occult:`оккультный`, :t_divination:`прорицание`, :t_detection:`обнаружение`)
Ревенант знает направление в котором находится его убийца (пока оба находятся на одном плане), но не расстояние.

----------

**КБ**: 34;
**Стойкость**: +22,
**Рефлекс**: +24,
**Воля**: +22

**ОЗ**: 220,
:ref:`негативное исцеление <cr_ability--Negative-Healing>`

**Иммунитеты**:
эффекты :t_death:`смерти`,
:t_sleep:`сон`,
:t_poison:`яд`,
:t_disease:`болезнь`,
:c_paralyzed:`парализован`

**Сопротивления**: физический 15 (кроме рубящего)


**Аура тишины (Silent Aura)**
(:t_aura:`аура`, :t_occult:`оккультный`, :t_illusion:`иллюзия`)
10 футов.
Тихий преследователь не издает звука, что не дает существам заметить его используя только слух или аналогичное чувство.
Эта тишина останавливает все звуки внутри нее или проходящие сквозь нее.
Тихий преследователь и все существа в ауре не могут использовать звуковые атаки или действия, имеющие :t_auditory:`слуховой` признак; это не позволяет существам сотворять заклинания со :ref:`словесными компонентами <ch7--Spell-Component--Verbal>` или активировать предметы с :ref:`командными компонентами <Activate-an-Item--Component--Command>`.


**Ненависть к себе (Self-Loathing)**
(:t_visual:`визуальный`, :t_emotion:`эмоция`, :t_mental:`ментальный`)
Если ревенант видит свое отражение или какой-либо объект, который был важен для него в жизни, то он должен совершить спасбросок Воли КС 33.

| **Критический успех**: Ревенант невредим и больше не может быть подвержен отражению или объекту таким образом.
| **Успех**: Ревенант отвлечен ненавистью к себе и становится :c_slowed:`замедлен 1` на 1 раунд.
| **Провал**: Ревенант становится :c_fascinated:`заворожен` источником спровоцировавшим его ненависть к себе и до конца своего следующего хода делает всё, чтобы уничтожить его.
| **Критический провал**: Ревенант становится :c_immobilized:`обездвижен` пока присутствует источник его ненависти к себе, пока он не атакован или не увидит своего убийцу.


**Бессмертная вендетта (Undying Vendetta)**
(:t_occult:`оккультный`, :t_necromancy:`некромантия`, :t_emotion:`эмоция`)
Если убийца ревенанта умирает, то ревенант немедленно уничтожается.
Ревенант, который не может ощущать своего убийцу, должен раз в 24 часа совершать чистую проверку КС 11, чтобы не стать :c_immobilized:`обездвиженным` и :c_prone:`ничком`; он немедленно восстает, как только почувствует своего убийцу.
Убийца, ставший :t_undead:`нежитью`, не вызывает уничтожения ревенанта до тех пор, пока убийца не будет окончательно уничтожен.
Ревенант получает бонус состояния +2 к проверкам и КС против своего убийцы.

----------

**Скорость**: 25 футов


**Ближний бой**: |д-1| коготь +27 [+23/+19] (:w_agile:`быстрое`),
**Урон** 3d8+14 дробящий + :ref:`cr_ability--Grab`


:ref:`cr_ability--Constrict` |д-1| 3d8+7 дробящий, КС 33


**Неумолимое нападение (Implacable Advance)** |д-2|
Тихий преследователь :ref:`Перемещается (Strides) <action--Stride>` дважды и наносит Удар когтями; если во время этих Перемещений он продвигается к своему убийце, то может игнорировать сложную местность.
Если в этом ходу он Ударяет и :ref:`Захватывает (Grab) <cr_ability--Grab>` своего убийцу, то сразу после этого Захвата может :ref:`cr_ability--Constrict` свободным действием (|д-св|).





.. include:: /helpers/actions.rst
.. include:: /helpers/bestiary-icons.rst